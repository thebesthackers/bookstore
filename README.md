**Specyfikacja wymagań projektu:
**https://docs.google.com/document/d/19FwtpitBTVsvjSTSqJs3fF96Zbe2FVj44fR5uhEldT8/edit

**Product Backlog:**
https://docs.google.com/document/d/1nRzAL05JebhPIvLJ0H58iVSUcfqkiKa1rfa3mqENpOs/edit

**Analiza systemowa:**
https://docs.google.com/document/d/1HaLLp0kKn2mSqgOLlv34xhX1ptVKSXypRYyZi5AJ090/edit

Przykład kodu, który jest odpowiedzialny za pobranie z bazy obrazka i dodanie go do panelu:
http://pastebin.com/MXBhe0vJ


**Konfiguracja eclipse:
**
Windows -> Preferences -> Maven -> Discovery -> Open Catalog -> instalujemy m2e-egit

File -> Import -> Projects from Git -> Clone URI ->
URI: https://grzego131@bitbucket.org/thebesthackers/bookstore.git
w Authentication podajecie swój login i hasło 
-> Next -> branch 1.02-basic-functionality (na tym będziemy jutro pracować)
-> Finish -> pojawia się nowe okno ->
Maven -> Check out Maven Projects from SCM -> SCM URL: git 
wklejamy adres: https://grzego131@bitbucket.org/thebesthackers/bookstore.git  -> Finish

Jak ktoś chce pisać w intellij to prosze o kontakt :)