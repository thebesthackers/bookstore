package com.bookstore.utils;


import com.bookstore.entity.Product;
import com.bookstore.entity.Sale;
import com.bookstore.entity.User;
import com.bookstore.enums.Access;

import org.hibernate.Session;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public final class InsertUtil {
	
	public static String DATA_FILE_DIRECTORY = "product_data/";
	public static String DATA_FILE_PATH = DATA_FILE_DIRECTORY+"products.csv";

    private InsertUtil() {}

    public static void main(String[] args) {
        insert();
        //loadDataFromCsv();
    }

    /*
     * Przykladowy kod wpisu do bazy, klikamy prawym przyciskiem myszy na klase
     * i dajemy opcje Run albo Run As -> Java Application w zaleznosci od IDE
     *
     * Wpiszcie sobie dane jakie potrzebujecie do testowania
     * id nie podajemy (hibernate generuje je automatycznie)
     */
    private static void insert() {
        Sale sale = new Sale();
        sale.setSeller("Grzes");

        Sale sale2 = new Sale();
        sale2.setSeller("Justyna");

        Product product = new Product();
        product.setName("Product1");
        product.setPosition("4");
        product.setPrice(BigDecimal.valueOf(20));
        product.setQuantity(25);
        product.setImage(getImageFromFile("nazwa zdjecia lub lokalizacja.png"));

        product.getSale().add(sale);
        product.getSale().add(sale2);
        
        User user = new User();
        user.setLogin("SELLER");
        user.setPassword("haslo1");
        user.setAccess(Access.SELLER.toString());
        user.setName("SELLER");
        
        User user2 = new User();
        user2.setLogin("ACCOUNTANT");
        user2.setPassword("haslo2");
        user2.setAccess(Access.ACCOUNTANT.toString());
        user2.setName("ACCOUNTANT");
        
        User user3 = new User();
        user3.setLogin("ADMIN");
        user3.setPassword("haslo3");
        user3.setAccess(Access.ADMIN.toString());
        user3.setName("ADMIN");
        
        Session session = HibernateUtil.getSession();
        session.beginTransaction();

        session.save(product);
        session.save(sale);
        session.save(sale2);
        session.save(user);
        session.save(user2);
        session.save(user3);

        session.getTransaction().commit();
        session.close();
    }
    
    /*
     * Method for populating database with values from CSV file
     * 
     */
    private static void loadDataFromCsv(){
    	File dataFile = new File(DATA_FILE_PATH);
    	BufferedReader bufReader = null;
    	String line;
    	String[] productData;
    	List<Product> productsList = new ArrayList<Product>();
    	try{
    		bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(dataFile)));
    		//Skipping headerLine
    		bufReader.readLine();
    		while ((line = bufReader.readLine()) != null) {
    			productData = line.split(",");
                Product product = new Product();
                product.setName(productData[0]);
                product.setPosition(productData[3]);
                product.setPrice(new BigDecimal(productData[1]));
                product.setQuantity(Integer.parseInt(productData[2]));
                product.setImage(getImageFromFile(DATA_FILE_DIRECTORY+productData[4]));
                productsList.add(product);
    		}
        }catch(NumberFormatException ex){
        	System.err.println("loadDataFromCsv NumberFormatException "+ex.getMessage());
    	}catch(FileNotFoundException ex){
    		System.err.println("Data file not found at path: "+DATA_FILE_PATH);
    	}catch(IOException ex){
    		System.err.println("loadDataFromCsv IOException "+ex.getMessage());
    	}
    	insertProducts(productsList);
    }
    
    private static void insertProducts(List<Product> productsList){
    	Session session = HibernateUtil.getSession();
        session.beginTransaction();

        for(Product product : productsList){
        	session.save(product);
        }

        session.getTransaction().commit();
        session.close();
    }

    private static byte[] getImageFromFile(String imageName) {
        File file = new File(imageName);
        byte[] bFile = new byte[(int) file.length()];

        try {
            FileInputStream inputStream = new FileInputStream(file);
            inputStream.read(bFile);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bFile;
    }
}
