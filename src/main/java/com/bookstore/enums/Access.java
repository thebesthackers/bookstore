package com.bookstore.enums;


public enum Access {
    ADMIN,
    SELLER,
    ACCOUNTANT
}
