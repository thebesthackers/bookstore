package com.bookstore.entity;


import javax.persistence.*;

@Entity
@Table(name = "USERS")
@NamedQueries({
    @NamedQuery(name = User.FIND_USERS_BY_NAME, query = "SELECT u FROM User u WHERE u.name LIKE :name")
})

public class User {
	
	public final static String FIND_USERS_BY_NAME = "User.findByName";

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;
    private String name;
    private String password;
    private String login;
    private String access;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }
}
