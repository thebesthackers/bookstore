package com.bookstore.entity;


import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "SALES")
public class Sale {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int saleId;
    private Date date;
    private String seller;
    private int quantity;

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
