package com.bookstore.entity;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "PRODUCTS")
@NamedQueries({
        @NamedQuery(name = Product.FIND_ALL_PRODUCTS, query = "SELECT p FROM Product p"),
        @NamedQuery(name = Product.FIND_PRODUCTS_BY_NAME, query = "SELECT p FROM Product p WHERE p.name LIKE :name")
})
public class Product {
	
	public final static String FIND_ALL_PRODUCTS = "Product.findAll";
	public final static String FIND_PRODUCTS_BY_NAME = "Product.findByName";

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int productId;
    private String name;
    private BigDecimal price;
    private String position;
    private int quantity;
    private byte[] image;
    @OneToMany
    private Collection<Sale> sale = new ArrayList<Sale>();

    public int getProductId() {
        return productId;
    }

    public void setProductId(int id) {
        this.productId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Collection<Sale> getSale() {
        return sale;
    }
}
