package com.bookstore.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.engine.jdbc.connections.internal.UserSuppliedConnectionProviderImpl;

import com.bookstore.entity.Product;
import com.bookstore.entity.User;
import com.bookstore.utils.HibernateUtil;

public class ProductDao {
	
	public static List<Product> getAllProducts(){
		Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.getNamedQuery(Product.FIND_ALL_PRODUCTS);
        return query.list();
	}
	
	public static List<Product> getProductsByName(String productName){
		Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.getNamedQuery(Product.FIND_PRODUCTS_BY_NAME).setString("name", productName);
        return query.list();
	}
	
	public static List<User> getUsersByName(String userName){
		Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.getNamedQuery(User.FIND_USERS_BY_NAME).setString("name", userName);
        return query.list();
	}
}
