package com.bookstore.frames;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SearchPanel extends JPanel implements ActionListener {
	
	public static String SEARCH_BUTTON_ACTION_COMMAND = "search_button";
	
	JLabel searchLabel;
	JButton searchButton;
	JTextField searchInput;

	public SearchPanel(){
		searchLabel = new JLabel("Szukany produkt");
		
		searchButton = new JButton("Szukaj");
		searchButton.setActionCommand(SEARCH_BUTTON_ACTION_COMMAND);
		searchButton.addActionListener(this);
		searchInput = new JTextField();
		searchInput.setColumns(20);
		
		this.add(searchLabel);
		this.add(searchInput);
		this.add(searchButton);
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals(SEARCH_BUTTON_ACTION_COMMAND)){
			String productName = searchInput.getText()+"%";
			((MainFrame)SwingUtilities.getWindowAncestor(this)).reloadList(productName);;
		}
		
	}
	
	
}
