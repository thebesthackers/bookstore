package com.bookstore.frames;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.bookstore.entity.Product;
import com.bookstore.dao.ProductDao;

public class SearchResultsPanel extends JPanel {

	List<Product> searchResultsList;
	
	
	void generateDummyList()
	{
		
		searchResultsList = new ArrayList<Product>();
		
		for(int k = 0;k<6;k++)
		{
		Product dummyArticle = new Product();
		dummyArticle.setName("Ksi��ka");
		dummyArticle.setPrice(new BigDecimal(29.99));
		dummyArticle.setPosition("B13");
		dummyArticle.setQuantity(2);
		searchResultsList.add(dummyArticle);
		dummyArticle = new Product();
		dummyArticle.setName("P�yta");
		dummyArticle.setPrice(new BigDecimal(19.99));
		dummyArticle.setPosition("F9");
		dummyArticle.setQuantity(9);
		searchResultsList.add(dummyArticle);
		
		dummyArticle = new Product();
		dummyArticle.setName("Komiks");
		dummyArticle.setPrice(new BigDecimal(39.99).setScale(2, BigDecimal.ROUND_DOWN));
		dummyArticle.setPosition("A2");
		dummyArticle.setQuantity(1);
		searchResultsList.add(dummyArticle);
		}

		
	}
	public SearchResultsPanel() {
		
		searchResultsList = ProductDao.getAllProducts();
		
		//generateDummyList();
		
		setLayout(new GridLayout(searchResultsList.size(),5));
		
		JLabel labelArticleLp;
		JLabel labelArticleName;
		JLabel labelArticlePrice;
		JLabel labelArticlePosition;
		JLabel labelArticleQuantity;
		JButton productSellButton;
		
        for(int i = 0; i< searchResultsList.size();i++)
        {

        String strValue = String.valueOf(i+1);	
        labelArticleLp = new JLabel(strValue, JLabel.CENTER);
        setBgColor(i,labelArticleLp);

        labelArticleName = new JLabel(searchResultsList.get(i).getName(),JLabel.CENTER);
        setBgColor(i,labelArticleName);
        
    	DecimalFormat df = new DecimalFormat();
    	df.setMaximumFractionDigits(2);
        strValue =  df.format(searchResultsList.get(i).getPrice());
        labelArticlePrice = new JLabel(strValue, JLabel.CENTER);
      
        setBgColor(i,labelArticlePrice);
        
        labelArticlePosition = new JLabel(searchResultsList.get(i).getPosition(), JLabel.CENTER);
        setBgColor(i,labelArticlePosition);
        
        strValue = String.valueOf(searchResultsList.get(i).getQuantity());	
        labelArticleQuantity = new JLabel(strValue, JLabel.CENTER);
        setBgColor(i,labelArticleQuantity);
        
        productSellButton = new JButton("Sprzeda�");
        
        add(labelArticleLp);
        add(labelArticleName);
        add(labelArticlePrice);
        add(labelArticlePosition);
        add(labelArticleQuantity);
        add(productSellButton);
        
        
        setVisible(true);
        }

    }
	
	public void setBgColor(int i,JLabel l)
	{
		if(i%2==0)
		{
    		l.setOpaque(true);
    		l.setBackground(Color.lightGray);
		}
		if(i==-1)
		{
    		l.setOpaque(true);
    		l.setBackground(Color.gray);
		}

	        l.setVerticalTextPosition(JLabel.BOTTOM);
	        l.setHorizontalTextPosition(JLabel.CENTER);
	}
	
	
	
}
