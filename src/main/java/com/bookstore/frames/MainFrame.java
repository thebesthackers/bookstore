package com.bookstore.frames;


import javax.swing.*;

import com.bookstore.dao.ProductDao;

import java.awt.*;

public class MainFrame extends JFrame {
	
    public MainFrame() {
        super("Bookstore");
        
        setSizeAndLocation();
        setLookAndFeel();
        initComponents();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          
    }

    private void setSizeAndLocation() {
    	
    	FlowLayout layout = new FlowLayout ();
    	this.setLayout(layout);
    	
        int widthScreen = Toolkit.getDefaultToolkit().getScreenSize().width;
        int heightScreen = Toolkit.getDefaultToolkit().getScreenSize().height;
        this.setSize((widthScreen - 300), (heightScreen - 200));

        int widthFrame = this.getSize().width;
        int heightFrame = this.getSize().height;
        this.setLocation((widthScreen - widthFrame) / 2, (heightScreen - heightFrame) / 2);
    }

    private void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initComponents() {
    	initLoginPanel();
    }
    
    public void initLoginPanel(){
    	this.add(new LoginPanel());
    	pack();
    }
    
    public void initSearchPanel(){
    	this.add(new SearchPanel());
        setSizeAndLocation();
    }
    
    public void initSearchResultsPanel()
    {
    	

    	SearchResultsPanel searchResultsPanel = new SearchResultsPanel();
        JScrollPane scroller = new JScrollPane(searchResultsPanel);

        
        JPanel naglowek = new JPanel();
        GridLayout layout = new GridLayout(1,5);
    	naglowek.setLayout(layout);
    	naglowek.setPreferredSize(new Dimension(this.getWidth()-48,50));
    	
        if(searchResultsPanel.searchResultsList.size()<15)
        {
        	scroller.setPreferredSize(new Dimension(this.getWidth()-50,searchResultsPanel.searchResultsList.size()*23+2));
        }
        else
        {
        	 scroller.setPreferredSize(new Dimension(this.getWidth()-50,360));
        }
   
		JLabel labelArticleLp;
		JLabel labelArticleName;
		JLabel labelArticlePrice;
		JLabel labelArticlePosition;
		JLabel labelArticleQuantity;
		JLabel labelProductSell;
		
        labelArticleLp = new JLabel("Lp", JLabel.CENTER);
        searchResultsPanel.setBgColor(-1,labelArticleLp);

        labelArticleName = new JLabel("Nazwa towaru",JLabel.CENTER);
        searchResultsPanel.setBgColor(-1,labelArticleName);
        labelArticlePrice = new JLabel("Cena", JLabel.CENTER);
        searchResultsPanel.setBgColor(-1,labelArticlePrice);
        
        labelArticlePosition = new JLabel("Pozycja w magazynie", JLabel.CENTER);
        searchResultsPanel.setBgColor(-1,labelArticlePosition);

        labelArticleQuantity = new JLabel("Ilo�� w magazynie", JLabel.CENTER);
        searchResultsPanel.setBgColor(-1,labelArticleQuantity);
        labelProductSell = new JLabel("Sprzeda�/Dostawa", JLabel.CENTER);
        searchResultsPanel.setBgColor(-1,labelProductSell);

        JLabel stopka = new JLabel("Nie ma wi�cej wynik�w do wy�wietlenia",JLabel.CENTER);

        naglowek.add(labelArticleLp);
        naglowek.add(labelArticleName);
        naglowek.add(labelArticlePrice);
        naglowek.add(labelArticlePosition);
        naglowek.add(labelArticleQuantity);
        naglowek.add(labelProductSell);
        add(naglowek);
        add(scroller);
        add(stopka);
    }
    
    public void reloadList(String productName){
    	ProductDao.getProductsByName(productName);
    	//TODO call method refreshing search result panel
    }
}
